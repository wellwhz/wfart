package cn.wellwhz.listener;

import cn.nukkit.Player;
import cn.nukkit.Server;
import cn.nukkit.block.Block;
import cn.nukkit.block.BlockCrops;
import cn.nukkit.block.BlockID;
import cn.nukkit.event.EventHandler;
import cn.nukkit.event.Listener;
import cn.nukkit.event.block.BlockGrowEvent;
import cn.nukkit.event.player.PlayerToggleSneakEvent;
import cn.nukkit.level.Position;
import cn.nukkit.utils.Config;
import cn.wellwhz.WFart;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class PlayerFartListener implements Listener {
    public HashMap<String, Long> playerUseTime = new HashMap<>();

    @EventHandler
    private void onPlayerToggleSneak(PlayerToggleSneakEvent event) {
        Player p = event.getPlayer();

        //防止反复触发
        if (playerUseTime.get(p.getName()) != null && System.currentTimeMillis() - playerUseTime.get(p.getName()) < 150)
            return;
        playerUseTime.put(p.getName(), System.currentTimeMillis());

        Config config = WFart.config;
        Position pos = event.getPlayer().getPosition();
        for (double i = -config.getDouble("最大庄稼催熟距离"); i <= config.getDouble("最大庄稼催熟距离"); i++) {
            for (double j = -config.getDouble("最大庄稼催熟距离"); j <= config.getDouble("最大庄稼催熟距离"); j++) {
                for (double k = -config.getDouble("最大庄稼催熟距离"); k <= config.getDouble("最大庄稼催熟距离"); k++) {
                    double dis = Math.sqrt(Math.pow(i, 2) + Math.pow(j, 2) + Math.pow(k, 2));
                    if (dis >= config.getDouble("最小庄稼催熟距离") && dis <= config.getDouble("最大庄稼催熟距离")) {
                        Block block = pos.add(i, j, k).getLevelBlock();
                        if (block instanceof BlockCrops) {
                            if (((BlockCrops) block).getGrowth() < ((BlockCrops) block).getMaxGrowth()) {
                                block.add(Math.random(), Math.random(), Math.random()).level.addParticleEffect(block.add(Math.random(), Math.random(), Math.random()).asVector3f(), "minecraft:villager_happy", -1L, block.add(Math.random(), Math.random(), Math.random()).level.getGenerator().getDimension(), (Player[]) null);
                                if (Math.random() < config.getDouble("庄稼催熟概率")) {
                                    BlockCrops block1 = (BlockCrops) block.clone();
                                    int growth = block1.getGrowth();
                                    block1.setGrowth(growth + 1);
                                    BlockGrowEvent ev = new BlockGrowEvent(block, block1);
                                    Server.getInstance().getPluginManager().callEvent(ev);
                                    block.getLevel().setBlock(block, ev.getNewState(), false, true);
                                }
                            }
                        }
                    }
                }
            }
        }
        for (double i = -config.getDouble("最大树苗催熟距离"); i <= config.getDouble("最大树苗催熟距离"); i++) {
            for (double j = -config.getDouble("最大树苗催熟距离"); j <= config.getDouble("最大树苗催熟距离"); j++) {
                for (double k = -config.getDouble("最大树苗催熟距离"); k <= config.getDouble("最大树苗催熟距离"); k++) {
                    double dis = Math.sqrt(Math.pow(i, 2) + Math.pow(j, 2) + Math.pow(k, 2));
                    if (dis >= config.getDouble("最小树苗催熟距离") && dis <= config.getDouble("最大树苗催熟距离")) {
                        Block block = pos.add(i, j, k).getLevelBlock();
                        if (block.getId() == BlockID.SAPLING) {
                            block.add(Math.random(), Math.random(), Math.random()).level.addParticleEffect(block.add(Math.random(), Math.random(), Math.random()).asVector3f(), "minecraft:villager_happy", -1L, block.add(Math.random(), Math.random(), Math.random()).level.getGenerator().getDimension(), (Player[]) null);
                            if (Math.random() < config.getDouble("树苗催熟概率")) {
                                try {
                                    Method growMethod = block.getClass().getDeclaredMethod("grow", (Class<?>) null);
                                    growMethod.setAccessible(true);
                                    growMethod.invoke(block);
                                } catch (InvocationTargetException | IllegalAccessException | NoSuchMethodException ignored) {
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
